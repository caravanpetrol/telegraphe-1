<div id="audio-player" class="uk-height-xsmall ">
  <div uk-grid>
    <div class="uk-width-auto uk-visible@s uk-padding-remove-left@-m">
      <img data-src="assets/residence-pochette.png" width="90 " height="90 " alt="pochette " uk-img>
    </div>
    <div class="uk-width-expand uk-child-width-1-1">

      <!-- PROGRESS BAR -->
      <div class="uk-padding-xsmall-v">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
          <div class="">
            <span class="audio-time">0:15</span>
          </div>
          <div class="uk-width-expand">
            <progress id="js-progressbar" class="uk-progress" value="10" max="100"></progress>
          </div>
          <div class=" ">
            <div>
              <span class="audio-duration">3:53</span>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN PROGRESS BAR -->

      <!-- TEXT+BOUTONS -->
      <div>
        <div class="uk-flex uk-flex-middle" uk-grid>
          <div class="uk-width-expand uk-width-1-2@m uk-margin-small-right">
            <!-- uk-width-1-2 uk-width-expand@s uk-width-1-2@m-->
            <div>
              <p class="uk-margin-remove uk-text-bold uk-text-truncate">
                ArtisteArtiste Artiste ArtisteArtiste<br>Titre TitreTitre TitreTitreTitre
              </p>
            </div>
          </div>
          <div class="uk-width-auto uk-flex uk-flex-right uk-padding-remove-left@-m">
            <!-- uk-width-expand -->
            <div class="uk-text-center">
              <a class="uk-button uk-button-default uk-border-rounded uk-text-uppercase uk-text-xsmall uk-margin-small-right uk-visible@s"
                href="# ">Acheter</a>
              <div class="uk-inline">
                <button class="uk-button uk-button-secondary uk-border-rounded uk-text-uppercase uk-text-xsmall uk-margin-small-right uk-visible@s" type="button">Partager</button>
                <div uk-drop="mode: click; pos: top-center">
                    <div class="uk-background-secondary uk-padding-xsmall uk-border-rounded">
                      <?php include("partiel/share-h.php"); ?>
                    </div>
                </div>
              </div>

              <a id="btn-audio-prev " class="uk-width-xxsmall " href="# " uk-icon="icon: ico-play-prev; ratio: .8 "></a>
              <a id="btn-audio-play " class="uk-width-xxsmall " href="# " uk-icon="icon: ico-play; ratio: 1 "></a>
              <a id="btn-audio-pause " class="uk-width-xxsmall uk-hidden " href="# " uk-icon="icon: ico-pause; ratio: 1 "></a>
              <a id="btn-audio-next " class="uk-width-xxsmall " href="# " uk-icon="icon: ico-play-next; ratio: .8 "></a>

            </div>
          </div>
        </div>
      </div>
      <!-- FIN TEXT+BOUTONS -->

    </div>

  </div>
</div>
