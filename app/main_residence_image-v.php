<picture class="uk-full-width-height">
  <source media="(max-width: 639px)" srcset="https://fakeimg.pl/640x850">
  <source media="(min-width: 640px) and (max-width: 959px)" srcset="https://fakeimg.pl/960x1280">
  <source media="(min-width: 960px) and (max-width: 1199px)" srcset="https://fakeimg.pl/1200x1600">
  <source media="(min-width: 1200px)" srcset="https://fakeimg.pl/1800x2400">
  <img src="https://fakeimg.pl/960x720" alt="nom de l'evenement">
</picture>
