<div class="uk-padding-left@m" uk-height-viewport="expand: true">
  <div  class="uk-container uk-container-expand" >
    <h2>buttons</h2>
    <a href="#" class="uk-button uk-button-default uk-margin-small-h">uk-button uk-button-default</a>
    <a href="#" class="uk-button uk-button-primary uk-margin-small-h">uk-button uk-button-primary</a>
    <a href="#" class="uk-button uk-button-secondary uk-margin-small-h">uk-button uk-button-secondary</a>
    <a href="#" class="uk-button uk-button-danger uk-margin-small-h">uk-button uk-button-danger</a>
    <div class="uk-light">
      <a href="#" class="uk-button uk-button-default uk-margin-small-h">INV uk-button uk-button-default</a>
    </div>
  </div>
</div>
