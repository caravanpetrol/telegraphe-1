<header class="uk-width-1-1">

    <!-- Nav secondaire (gauche) -->
    <nav id="secondary-nav" class="uk-visible@m uk-width-xsmall uk-position-fixed uk-position-z-index uk-height-viewport uk-flex uk-flex-center uk-flex-middle"
    >
      <ul class="uk-iconnav uk-iconnav-vertical uk-text-center">

        <li>
          <a href="podcast.php">
            <span uk-icon="icon: ico-podcasts; ratio: 1"></span>
          </a>
        </li>
        <li class="">
          <a href="agenda.php">
            <span uk-icon="icon: ico-agenda; ratio: 1.1"></span>
          </a>
        </li>
        <li class="">
          <a href="shop.php">
            <span uk-icon="icon: ico-logo-editions-telegraphe; ratio: 1.7"></span>
          </a>
        </li>



      </ul>
    </nav>

    <!-- Nav primaire (haut) -->
    <nav id="primary-nav" class="uk-visible@m uk-padding-left@m uk-navbar-container uk-height-xsmall uk-navbar-transparent" uk-navbar>
      <div class="uk-navbar-right">

            <ul class="uk-navbar-nav uk-position-absolute">
              <li>
                <span class="uk-navbar-item" uk-icon="icon: ico-logo-telegraphe-2; ratio: 4.5"></span>
                <!--<a href="podcast.php">podcast</!--<a>-->
              </li>

            </ul>
      </div>

    </nav>

    <!-- Nav mobile -->
    <div id="mobile_nav" class="uk-offcanvas-content uk-hidden@m">
      <nav class="uk-navbar uk-navbar-container uk-navbar-transparent">
        <div class="uk-navbar-left" style="position: relative;top: 2rem;left: 1rem;">
          <span class="uk-navbar-item" uk-icon="icon: ico-logo-telegraphe-2; ratio: 4"></span>
        </div>
        <div class="uk-navbar-right">
          <a class="uk-navbar-toggle" uk-icon="icon: menu; ratio: 1.5" href="#primary-mobile-nav" uk-toggle></a>
        </div>
      </nav>

      <div id="primary-mobile-nav" uk-offcanvas>
        <div class="uk-offcanvas-bar">
          <button class="uk-offcanvas-close uk-close-large" type="button" uk-close></button>
          <span class="uk-navbar-item" uk-icon="icon: ico-logo-telegraphe; ratio: 3"></span>
          <br>
          <ul class="uk-nav uk-nav-center">
            <li>
              <a href="#">Le fil</a>
            </li>
            <li class="uk-active">
              <a href="agenda.php">Agenda</a>
            </li>
            <li>
              <a href="collection.php">Collection</a>
            </li>
            <li>
              <a href="telegraphe.php">Le telegraphe</a>
            </li>
            <li>
              <a href="#">Abonnement</a>
            </li>
            <li>
              <a href="#">S'identifier</a>
            </li>
          </ul>
        </div>
      </div>
    </div>



  </header>
