<script>
  const body = document.getElementById("index_v2")
  const hours = new Date().getHours()
  function hour(){
    // return '00'
    return hours
    // return new Date().getHours()
  }

  function isDay(){
    // console.log(`c'est le jour ? ${hour() > '08' && hour() < '20'}`)
    return hour() > '08' && hour() < '20'
  }

  // isDay()
  // console.log(`il est ${hour()}h`)

  // Activer le night mode (20h > 8h)
  hour() > '08' && hour() < '20' ? body.classList.remove("night") : body.classList.add("night")

  // Switch Newsletter input
  const newsletter_form = document.getElementById("new_contact")
  newsletter.addEventListener("click", function(){
    newsletter_form.classList.toggle("uk-hidden")
  })


  // IMAGES FADES ON ROLLOVER
  const image1 = new Image();
  const image2 = new Image();
  const image3 = new Image();
  const tagParent = document.getElementById("bg_2");
  image1.id = "bg_image_1";
  image2.id = "bg_image_2";
  image3.id = "bg_image_3";
  image1.className = "uk-visible@m";
  image2.className = "uk-visible@m";
  image3.className = "uk-visible@m";
  tagParent.appendChild(image1);
  tagParent.appendChild(image2);
  tagParent.appendChild(image3);

  const item_shop = document.getElementById("item-shop")
  item_shop.addEventListener("mouseover", function(){
    image1.onload = function() {
      this.classList.remove("fadeout")
      this.classList.add("fadein")
    }
    image1.src = isDay() ? 'assets/background-shop.jpg' : 'assets/background-shop-night.jpg'
  })
  item_shop.addEventListener("mouseout", function(){
    bg_image_1.classList.remove("fadein")
    bg_image_1.classList.add("fadeout")
  })

  const item_agenda = document.getElementById("item-agenda")
  item_agenda.addEventListener("mouseover", function(){
    image2.onload = function() {
      this.classList.remove("fadeout")
      this.classList.add("fadein")
     }
    image2.src = isDay() ? 'assets/background-agenda.jpg' : 'assets/background-agenda-night.jpg'
  })
  item_agenda.addEventListener("mouseout", function(){
    bg_image_2.classList.remove("fadein")
    bg_image_2.classList.add("fadeout")
  })

  const item_podcasts = document.getElementById("item-podcasts")
  item_podcasts.addEventListener("mouseover", function(){
    image3.onload = function() {
      this.classList.remove("fadeout")
      this.classList.add("fadein")
    }
    image3.src = isDay() ? 'assets/background-podcast.jpg' : 'assets/background-podcast-night.jpg'
  })
  item_podcasts.addEventListener("mouseout", function(){
    bg_image_3.classList.remove("fadein")
    bg_image_3.classList.add("fadeout")
  })


</script>
