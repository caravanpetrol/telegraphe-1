<footer class="uk-grid-collapse uk-child-width-auto@m uk-flex-middle uk-grid-match
    uk-margin-remove-top" uk-grid>

    <div class=" uk-margin-remove uk-padding-remove-left@m">
      <div class="uk-flex uk-flex-center uk-flex-right@m ">
        <nav class="uk-navbar ">
          </nav>
        </div>
      </div>

      <div class="uk-visible@m">
        <div class="uk-flex uk-flex-center uk-flex-right@m uk-margin-small-top@-m">
          <nav class="uk-navbar">
            <ul class="uk-navbar-nav uk-navbar-nav-footer uk-display-inline-block ">
              <li class="uk-display-inline-block">
                <a id="newsletter" class="uk-display-inline" href="#">newsletter</a>
                <form data-parsley-validate class="uk-display-inline simple_form new_contact uk-hidden" id="new_contact" action="/contacts" accept-charset="UTF-8" method="post"
                  _lpchecked="1">
                  <input name="utf8" type="hidden" value="✓">
                  <input type="hidden" name="return_path" value="/-/inscription-newsletter">
                  <input type="email" name="contact[email]" required placeholder="votre email" class="uk-input uk-form-width-small" uk-toggle="cls: uk-form-width-medium; mode: media; media: @s" />
                  <input type="hidden" name="contact[subject]" value="Inscription NewsLetter" />
                  <input type="hidden" name="contact[text]" value="Inscription NewsLetter" />
                  <input type="hidden" name="contact[name]" value="Inscription NewsLetter" />
                  <span><button value="OK" class="uk-button">OK</button></span>
                </form>
              </li>

            <li class="uk-display-inline-block ">
              <a class="uk-padding-remove-h@-m" href="/fr/notre/faq">le telegraphe</a>
            </li>
            <li class="uk-display-inline-block ">
              <a href="#">
                <span uk-icon="icon: ico-fil; ratio: 2"></span>
              </a>
            </li>
            <li class="uk-display-inline-block ">
              <a href="#">
                <span uk-icon="icon: ico-connexion; ratio: 1.1"></span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>



    <div class="uk-width-1-1 uk-width-expand@m uk-margin-remove ">
      <div class="uk-flex uk-flex-center uk-flex-right@m ">
        <nav class="uk-navbar ">
          <ul class="uk-navbar-nav uk-iconnav ">
            <li>
              <a class="uk-padding-remove-h " href="https://www.scoop.it/topic/press-book-by-le-telegraphe-10" target="_blank">
                <span class=" " uk-icon="icon: ico-scoopit "></span>
              </a>
            </li>
            <li>
              <a class="uk-padding-remove-h " href="https://www.instagram.com/letelegraphetoulon/" target="_blank">
                <span class=" " uk-icon="icon: instagram "></span>
              </a>
            </li>
            <li>
              <a class="uk-padding-remove-h " href="https://www.pinterest.fr/letelegraphetoulon/?eq=letelegraphe&etslf=6736" target="_blank">
                <span class=" " uk-icon="icon: pinterest "></span>
              </a>
            </li>
            <li class=" ">
              <a class="uk-padding-remove-h " href="https://twitter.com/TelegrapheTln" target="_blank">
                <span class=" " uk-icon="icon: twitter "></span>
              </a>
            </li>
            <li>
              <a class="uk-padding-remove-left " href="https://www.facebook.com/LeTelegraphe.org/" target="_blank">
                <span class=" " uk-icon="icon: facebook "></span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </footer>
