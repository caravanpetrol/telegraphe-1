<footer class="uk-padding-left@m uk-grid-collapse uk-child-width-auto@m uk-flex-middle uk-grid-match
    uk-margin-remove-top uk-background-secondary" uk-grid>
    <div>
      <div class="uk-flex uk-flex-center uk-flex-right@m uk-margin-small-top@-m">
        <nav class="uk-navbar uk-text-uppercase">
          <ul class="uk-navbar-nav uk-navbar-nav-footer uk-display-inline-block ">
            <li class="uk-display-inline-block ">
              <a class="uk-padding-remove-h@-m" href="/fr/notre/faq">FAQ</a>
            </li>
            <li class="uk-display-inline-block ">
              <a href="/fr/prendre-un/abonnement">Abonnement</a>
            </li>
          </ul>
        </nav>
      </div>
    </div>

    <div class=" uk-margin-remove uk-padding-remove-left@m">
      <div class="uk-flex uk-flex-center uk-flex-right@m ">
        <nav class="uk-navbar ">
          <div class="uk-navbar-item ">
            <form data-parsley-validate class="simple_form new_contact" id="new_contact" action="/contacts" accept-charset="UTF-8" method="post"
              _lpchecked="1">
              <input name="utf8" type="hidden" value="✓">
              <input type="hidden" name="return_path" value="/-/inscription-newsletter">
              <input type="email" name="contact[email]" required placeholder="Newsletter" class="uk-input uk-form-width-small" uk-toggle="cls: uk-form-width-medium; mode: media; media: @s" />
              <input type="hidden" name="contact[subject]" value="Inscription NewsLetter" />
              <input type="hidden" name="contact[text]" value="Inscription NewsLetter" />
              <input type="hidden" name="contact[name]" value="Inscription NewsLetter" />
              <span class="uk-light"><button value="OK" class="uk-button uk-button-default">OK</button></span>
            </form>

          </div>
        </nav>
      </div>
    </div>

    <div class="uk-padding-remove-left uk-margin-remove  uk-text-center">
      <address class="uk-text-center">Toulon - 2 rue Hippolyte Duprat<br>contact@letelegraphe.org</address>
    </div>

    <div class="uk-width-1-1 uk-width-expand@m uk-margin-remove ">
      <div class="uk-flex uk-flex-center uk-flex-right@m ">
        <nav class="uk-navbar ">
          <ul class="uk-navbar-nav uk-iconnav ">
            <li>
              <a class="uk-padding-remove-h " href="https://www.instagram.com/letelegraphetoulon/" target="_blank">
                <span class="uk-icon-button " uk-icon="icon: instagram "></span>
              </a>
            </li>
            <li>
              <a class="uk-padding-remove-h " href="https://www.pinterest.fr/letelegraphetoulon/?eq=letelegraphe&etslf=6736" target="_blank">
                <span class="uk-icon-button " uk-icon="icon: pinterest "></span>
              </a>
            </li>
            <li class="uk-active ">
              <a class="uk-padding-remove-h " href="https://twitter.com/TelegrapheTln" target="_blank">
                <span class="uk-icon-button " uk-icon="icon: twitter "></span>
              </a>
            </li>
            <li>
              <a class="uk-padding-remove-left " href="https://www.facebook.com/LeTelegraphe.org/" target="_blank">
                <span class="uk-icon-button " uk-icon="icon: facebook "></span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </footer>
