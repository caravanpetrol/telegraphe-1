<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $page_title ?></title>
  <link href="https://fonts.googleapis.com/css?family=Oswald:200,400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:300,400" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="css/app.css">
  <?php include('inc/scripts.php'); ?>
</head>
