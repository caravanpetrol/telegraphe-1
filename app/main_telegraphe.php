
<main id="<?php echo $page_title ?>" class="uk-flex uk-flex-middle" uk-height-viewport="expand: true">
  <div class="uk-container uk-container-large">
    <div class="uk-flex uk-flex-middle uk-margin-bottom" uk-grid>

        <!-- LEFT -->
        <div class="uk-width-1-5 uk-width-small@l uk-visible@m uk-flex uk-flex-right">
          <!-- uk-width-auto@s -->
          <div>
            <ul class="uk-nav uk-nav-default uk-nav-compact">
              <li class="uk-active">
                <a href="#">Le telegraphe</a>
              </li>
              <li>
                <a href="#">Programmation</a>
              </li>
              <li>
                <a href="#">Le Bar</a>
              </li>
              <li>
                <a href="#">Abonnement</a>
              </li>
            </ul>
          </div>
        </div>

        <!-- MIDDLE -->
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-2-5@m uk-width-large@l uk-width-xlarge@xl">
          <div class="uk-text-center">
            <img data-src="https://fakeimg.pl/500x800" data-srcset="https://fakeimg.pl/400x300 959w,
            https://fakeimg.pl/500x800 960w" alt="" uk-img>
          </div>
        </div>

        <!-- RIGHT -->
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-2-5@m uk-width-large@l">
          <!-- uk-width-2-5@m uk-width-1-2@s -->
          <div>
            <h1>On va se réunir</h1>
            <p>Lorem ipsum dolor sit amet, consectetur
              <em>adipiscing elit, sed do eiusmod </em> tempor incididunt.</p>
            <p>Lorem ipsum dolor
              <strong>sit amet consectetur
                <a href="#">adipisicing elit.</a>
              </strong> Illo eligendi repellat rerum laborum facilis possimus id culpa pariatur libero totam aspernatur
              fugit voluptate explicabo nisi quis, suscipit, enim ad? Aliquid!</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate soluta perspiciatis, tempore commodi
              <a href="#">adipisicing elit.</a>
              hic voluptatibus</p>
          </div>
        </div>

      </div>
    </div>
  </div>

</main>

