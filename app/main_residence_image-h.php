<picture class="uk-full-width-height">
  <source media="(max-width: 639px)" srcset="https://fakeimg.pl/640x480">
  <source media="(min-width: 640px) and (max-width: 959px)" srcset="https://fakeimg.pl/960x720">
  <source media="(min-width: 960px) and (max-width: 1199px)" srcset="https://fakeimg.pl/1200x900">
  <source media="(min-width: 1200px)" srcset="https://fakeimg.pl/1800x1350">
  <img src="https://fakeimg.pl/960x720" alt="nom de l'evenement">
</picture>
