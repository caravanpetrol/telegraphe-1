<div class="uk-container uk-container-small-plus uk-padding-v@m uk-padding-remove-h@-m">
  <main id="<?php echo $page_title ?>" class="uk-child-width-1-1 uk-child-width-1-6@m uk-grid-small uk-grid-collapse@-m" uk-grid>

    <?php
      for ($i=0; $i < 15; $i++) {
        include('main_podcast_card.php');
      }
    ?>

  </main>
            <!-- AUDIO PLAYER -->
            <div class="uk-position-bottom uk-position-z-index uk-position-fixed uk-overlay uk-background-primary uk-padding-remove-v uk-push-right@m uk-padding-remove-left@-m">
          <?php include("main_podcast_audio-player.php"); ?>
          </div>
</div>
