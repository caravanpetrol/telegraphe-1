<section class="odd-even uk-margin-medium-v@m">
  <div class="uk-height-1-1 uk-padding-small-medium-v@-m">
    <div uk-grid class="uk-grid-collapse">

      <div class="uk-width-1-5 uk-width-1-1@m uk-flex uk-flex-middle uk-margin-small-medium-left@-m uk-margin-small-right@-m">
          <div class="uk-inline-clip uk-transition-toggle" tabindex="0">
            <img data-src="https://loremflickr.com/g/250/250/paris,girl/all?random=3" alt="" uk-img="" src="https://fakeimg.pl/300x220"
                width="250" height="250">
            <div class="uk-position-center">
              <a href="#"><span id="playIcon" class="uk-transition-fade uk-icon-button" uk-icon="icon: ico-play; ratio: 1"></span></a>
            </div>
          </div>
      </div>


      <div class="uk-width-expand uk-width-1-1@m uk-flex uk-flex-middle ">
          <div class="uk-padding-remove-horizontal uk-width-1-1 uk-margin-top-xsmall@m">

              <div class="uk-clearfix ">
                <div class="uk-float-right uk-margin-medium-h@-m">
                  <time datetime="2018-11-06 20:30" class="duration">
                    3:47
                  </time>
                </div>
                <div class="uk-float-left">
                  <time datetime="2018-11-06 20:30">
                    06-11-2018
                  </time>
                </div>
              </div>

              <h3 class="uk-margin-remove">Concert </h3>
              <h2 class="uk-margin-remove">
                <a class="" href="agenda-detail.php">Artiste ArtisteArtiste </a>
              </h2>
          </div>
      </div>


    </div>
  </div>
</section>

