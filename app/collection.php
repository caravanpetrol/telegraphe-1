<!DOCTYPE html>
<html lang="fr">

  <?php
    $page_title = "collection";
    include("inc/head.php");
   ?>

  <body class="<?php echo $page_title ?>">

    <?php
    include('inc/header.php');
    ?>

    <!-- CONTENT -->
    <div uk-grid class="wrapper" uk-height-viewport="expand: true">
      <div class="uk-width-1-1 content">
        <div class=''>
          <div class="uk-padding-left@m uk-width-1-1 uk-margin-remove-top">
            <?php include('main_' . $page_title . '.php'); ?>
          </div>
        </div>
      </div>
    </div>


    <?php include('inc/footer.php'); ?>
    <?php include('inc/scripts.php'); ?>



  </body>
</html>
