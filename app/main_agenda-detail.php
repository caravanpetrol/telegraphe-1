
    <main id="agenda-detail" class="uk-padding-left@m uk-width-1-1 uk-margin-remove-top">
      <!-- uk-margin-medium-large-left@m -->
      <div class="uk-flex uk-flex-middle" uk-height-viewport="expand: true">
        <div class="uk-container uk-container-large uk-padding-remove-h@-s">
          <div class="uk-flex uk-flex-middle uk-margin-bottom uk-grid-collapse" uk-grid>

            <!-- LEFT -->
            <div class="uk-width-1-1 uk-width-1-2@s uk-width-2-5@m uk-width-large@l uk-width-xlarge@xl">
              <div class="uk-text-center uk-inline">

                <picture>
                  <source media="(max-width: 639px)" srcset="https://fakeimg.pl/640x530">
                  <source media="(min-width: 640px)" srcset="https://fakeimg.pl/700x930">
                  <img src="https://fakeimg.pl/700x930" alt="nom de l'evenement">
                </picture>

                <div class="uk-position-right uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle uk-width-xsmall">
                  <?php include('partiel/share-v.php'); ?>
                </div>
              </div>
              <div class="uk-flex uk-flex-left uk-flex-center@s uk-margin-small-h uk-margin-small-top">
                <!-- ARROW NAV -->
                <?php include('partiel/arrow-nav.php') ?>
              </div>
            </div>



            <!-- RIGHT -->
            <div class="uk-width-1-1 uk-width-1-2@s uk-width-large@l">
              <article class="uk-width-max-xxlarge uk-margin-medium-left uk-margin-small-h@-s">

                <div class="">
                  <h1>Atelier percussion</h1>
                  <h2 class="uk-h3 uk-text-lowercase">Masterclass - Musique</h2>
                </div>
                <div uk-grid class="uk-grid-collapse uk-margin-top uk-flex uk-flex-wrap">
                  <div class="uk-width-auto">
                    <time datetime="2018-05-09 19:30 ">mercredi 9 mai 2018
                      <br>à 19h30
                    </time>
                  </div>
                  <div class="uk-width-expand uk-flex uk-flex-right uk-flex-middle"><!-- uk-margin-top@-m -->
                    <a class="uk-button uk-button-danger" href="#">
                      <span class="uk-icon" uk-icon="icon: ico-abonnement"></span><!-- uk-margin-small-right -->
                      réserver</a>
                  </div>
                </div>

                <p>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dignissimos nobis officia aliquid placeat nesciunt et itaque? Aspernatur
                  ipsam laudantium fugit praesentium voluptatem enim vel esse numquam tempora rerum. Consequuntur, accusantium!
                </p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum nisi ratione veniam quo architecto quaerat
                  ad, neque ea amet eos commodi consequatur quidem. Dolorum qui vel quo nemo saepe reprehenderit!</p>
              </article>
            </div>


          </div>
        </div>
      </div>
    </main>

