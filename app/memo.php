<!DOCTYPE html>
<html lang="fr">

  <?php
    $page_title = "memo";
    include("inc/head.php");
   ?>

  <body>

    <?php
    include('inc/header.php');
    ?>

    <!-- CONTENT -->
    <div uk-grid class="wrapper" uk-height-viewport="expand: true" style="background: #f2f2f2">
      <div class="uk-width-1-1 content">
        <div class='uk-width-1-1 uk-margin-top'>
          <?php include('main_' . $page_title . '.php'); ?>
        </div>
      </div>
    </div>


    <?php include('inc/footer.php'); ?>
    <?php include('inc/scripts.php'); ?>


  </body>
</html>


