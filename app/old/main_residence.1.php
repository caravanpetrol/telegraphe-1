          <main>

            <div id="<?php echo $page_title ?>" class="uk-flex uk-flex-center uk-child-width-1-1" uk-grid>

              <!-- TITLE -->
              <div uk-height-viewport="offset-top: 90px ">
                <div class="uk-flex uk-flex-center uk-text-center uk-margin-medium-large-top uk-light ">
                  <h1>
                    <span class="uk-heading-primary uk-display-block uk-margin-small-bottom">Le telegraphe</span>
                    <span class="uk-article-meta uk-display-block uk-margin-small-bottom">Collection juin 2018 #1</span>
                    <span class="uk-heading-hero uk-display-block">Brass</span>
                  </h1>
                </div>
              </div>

              <!-- HEADER IMAGE -->
              <div class="uk-position-fixed uk-padding-left@m" style="z-index: -1; padding-left: 0; left: 0">
                <!--  -->
                <!-- <div class="uk-card uk-card-default">card</div> -->
                <div uk-height-viewport="offset-top: 90px; " uk-parallax="blur: 15 " class=" uk-flex uk-flex-center uk-flex-middle uk-background-cover"
                  data-src="assets/residence-brass.jpg" uk-img>
                </div>
                <!--             <div uk-parallax="blur: 15 " class="uk-height-1-1 uk-flex uk-flex-center uk-flex-middle uk-background-cover" data-src="https://fakeimg.pl/800x600"
            uk-img>
          </div> -->
              </div>





              <!-- POSTS -->
              <div class="uk-background-primary ">
                <div class="uk-container uk-container-small ">
                  <section class="uk-section ">

                    <!-- TEXT MAX -->
                    <p uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">Lorem ipsum dolor,
                      sit amet consectetur adipisicing elit. Modi dicta est vel praesentium ipsum sequi quae ut ex. Ratione
                      incidunt consequuntur sed, aliquid eum recusandae voluptate minus vitae officiis nihil? Lorem ipsum
                      dolor sit amet consectetur adipisicing elit. Cum quibusdam esse rerum, corporis iure natus voluptas
                      nulla minima ea consequuntur, quaerat quod totam tempore, perspiciatis reiciendis commodi maxime eos
                      aliquam! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Blanditiis odio incidunt qui temporibus,
                      facere enim, quia dolor labore earum molestias ea odit assumenda officia praesentium autem soluta reiciendis
                      aut consequuntur.
                    </p>

                    <!-- TEXT LARGE -->
                    <div uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true " class="uk-flex-center
              uk-text-center uk-margin " uk-grid>
                      <p class="uk-width-large ">a consequuntur, quaerat quod totam tempore, perspiciatis reiciendis commodi
                        maxime eos aliquam! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Blanditiis odio incidunt
                        qui temporibus, facere enim, quia dolor labore earum molestias ea odit assumenda officia praesentium
                        autem soluta reiciendis aut consequuntur.</p>
                    </div>

                    <!-- TEXT MEDIUM -->
                    <div uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true " class="uk-flex-center
              uk-text-center uk-margin " uk-grid>
                      <p class="uk-width-medium ">a consequuntur, quaerat quod totam tempore, perspiciatis reiciendis commodi
                        maxime eos aliquam! Lorem, ipsum
                      </p>
                    </div>
                  </section>

                  <!-- IMAGE ONLY -->
                  <section class="uk-section uk-flex uk-flex-center ">
                    <div uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">
                      <img data-src="https://fakeimg.pl/850x500 " width="800 " height="600 " alt=" " uk-img>
                    </div>
                  </section>

                  <!-- IMAGE ONLY + PLAY AUDIO -->
                  <section class="uk-section uk-flex uk-flex-center ">
                    <div class="uk-inline " uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">
                      <img data-src="https://fakeimg.pl/850x500 " width="800 " height="600 " alt=" " uk-img>
                      <a class="uk-position-absolute uk-transform-center " style="right: 5px; bottom: 5px " href="#
              " uk-icon="icon: play-circle; ratio: 2 "></a>
                    </div>
                  </section>

                  <!-- IMAGE G + TEXTE D -->
                  <section class="uk-section ">
                    <div class="uk-flex uk-flex-middle uk-margin-bottom uk-child-width-1-1 uk-child-width-1-2@s " uk-grid>
                      <!-- RIGHT -->
                      <div class="uk-flex-last " uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dicta est vel praesentium ipsum
                          sequi quae ut ex. Ratione incidunt consequuntur sed, aliquid eum recusandae voluptate minus vitae
                          officiis nihil? Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum quibusdam esse rerum,
                          corporis iure natus voluptas nulla minima ea consequuntur, quaerat quod totam tempore, perspiciatis
                          reiciendis commodi maxime eos aliquam! Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                          Blanditiis odio incidunt qui temporibus, facere enim, quia dolor labore earum molestias ea odit
                          assumenda officia praesentium autem soluta reiciendis aut consequuntur.
                        </p>
                      </div>
                      <!-- LEFT -->
                      <div>
                        <div class="uk-inline " uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">
                          <img data-src="https://fakeimg.pl/350x450 " width="350 " height="450 " alt=" " uk-img>
                          <a class="uk-position-absolute uk-transform-center " style="right: 5px; bottom: 5px " href="#
              " uk-icon="icon: play-circle; ratio: 2 "></a>
                        </div>
                      </div>
                    </div>
                  </section>

                  <!-- IMAGE D + TEXTE G -->
                  <section class="uk-section ">
                    <div class="uk-flex uk-flex-middle uk-margin-bottom uk-child-width-1-1 uk-child-width-1-2@s " uk-grid>
                      <!-- LEFT -->
                      <div>
                        <div uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">
                          <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dicta est vel praesentium ipsum
                            sequi quae ut ex. Ratione incidunt consequuntur sed, aliquid eum recusandae voluptate minus vitae
                            officiis nihil? Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum quibusdam esse rerum,
                            corporis iure natus voluptas nulla minima ea consequuntur, quaerat quod totam tempore, perspiciatis
                            reiciendis commodi maxime eos aliquam! Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                            Blanditiis odio incidunt qui temporibus, facere enim, quia dolor labore earum molestias ea odit
                            assumenda officia praesentium autem soluta reiciendis aut consequuntur.
                          </p>
                        </div>
                      </div>
                      <!-- RIGHT -->
                      <div>
                        <div class="uk-inline " uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">
                          <img data-src="https://fakeimg.pl/350x450 " width="350 " height="450 " alt=" " uk-img>
                          <a class="uk-position-absolute uk-transform-center " style="right: 5px; bottom: 5px " href="#
              " uk-icon="icon: play-circle; ratio: 2 "></a>
                        </div>
                      </div>
                    </div>
                  </section>

                  <!-- SLIDESHOW (IN CONTAINER)-->
                  <section class="uk-section ">
                    <div class="uk-position-relative " uk-slideshow>
                      <ul class="uk-slideshow-items ">
                        <li>
                          <img src="https://fakeimg.pl/800x600 " alt=" " uk-cover>
                        </li>
                        <li>
                          <img src="https://fakeimg.pl/800x600 " alt=" " uk-cover>
                        </li>
                        <li>
                          <img src="https://fakeimg.pl/800x600 " alt=" " uk-cover>
                        </li>
                      </ul>
                      <div class="uk-position-bottom-center uk-position-small ">
                        <ul class="uk-dotnav ">
                          <li uk-slideshow-item="0 ">
                            <a href="# ">Item 1</a>
                          </li>
                          <li uk-slideshow-item="1 ">
                            <a href="# ">Item 2</a>
                          </li>
                          <li uk-slideshow-item="2 ">
                            <a href="# ">Item 3</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </section>
                </div>
              </div>

              <!-- SLIDESHOW FULL -->
              <div class="uk-background-primary uk-margin-remove uk-padding-medium-v ">
                <div class="uk-position-relative " uk-slideshow>
                  <ul class="uk-slideshow-items ">
                    <li>
                      <img src="https://fakeimg.pl/800x600 " alt=" " uk-cover>
                    </li>
                    <li>
                      <img src="https://fakeimg.pl/800x600 " alt=" " uk-cover>
                    </li>
                    <li>
                      <img src="https://fakeimg.pl/800x600 " alt=" " uk-cover>
                    </li>
                  </ul>
                  <div class="uk-position-bottom-center uk-position-small ">
                    <ul class="uk-dotnav ">
                      <li uk-slideshow-item="0 ">
                        <a href="# ">Item 1</a>
                      </li>
                      <li uk-slideshow-item="1 ">
                        <a href="# ">Item 2</a>
                      </li>
                      <li uk-slideshow-item="2 ">
                        <a href="# ">Item 3</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              <!-- DISQUE -->
              <div class="uk-background-primary uk-margin-remove uk-padding-medium-v ">
                <article class="uk-container uk-container-small ">
                  <div class="uk-child-width-1-2@s uk-child-width-1-1 uk-grid-large" uk-grid>
                    <div>
                      <div class="uk-card uk-card-media ">
                        <div class="uk-card-media-top ">
                          <div class="uk-box-shadow-bottom uk-box-shadow-small"> <img data-src="https://fakeimg.pl/155x155 "
                              width="155 " height="155 " alt=" " uk-img>
                          </div>
                        </div>
                        <div class="">
                          <table class="uk-table uk-table-xsmall uk-margin-top ">
                            <caption class="uk-margin-medium-top uk-margin-small-medium-bottom ">Label: LE TELEGRAPHE
                              <br>Format: Vinyl, LP, Album</caption>
                            <tbody>
                              <tr>
                                <td>track 1</td>
                                <td>02'20</td>
                              </tr>
                              <tr>
                                <td>track 2</td>
                                <td>15'50</td>
                              </tr>
                              <tr>
                                <td>track 3</td>
                                <td>5'10</td>
                              </tr>
                            </tbody>
                          </table>
                          <h4 class="uk-card-title ">Crédits</h4>
                          <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dicta est vel praesentium ipsum
                            sequi quae ut ex. Ratione incidunt consequuntur sed, aliquid eum recusandae voluptate minus vitae
                            officiis nihil? Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum quibusdam esse rerum,
                            corporis iure natus voluptas nulla minima ea consequuntur, quaerat quod totam tempore, perspiciatis
                            reiciendis commodi maxime eos aliquam! Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                            Blanditiis odio incidunt qui temporibus, facere enim, quia dolor labore earum molestias ea odit
                            assumenda officia praesentium autem soluta reiciendis aut consequuntur.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="uk-card">
                        <div class="uk-child-width-1-1 uk-child-width-1-2@m  uk-grid-collapse uk-margin-medium-bottom" uk-grid>
                          <div class="uk-table-border uk-padding-xsmall uk-margin-auto uk-width-max-medium">
                            <!-- style="max-width: 250px;" -->
                            <table class="uk-table uk-table-small uk-text-small uk-table-middle ">
                              <!-- <caption>Label Telegraphe</caption> -->
                              <tbody>
                                <tr>
                                  <td>Prix :</td>
                                  <td>68,00€</td>
                                </tr>
                                <tr>
                                  <td>Qté :</td>
                                  <td>
                                    <form class="uk-form-blank uk-width-xsmall">
                                      <!-- <input type="number"> -->
                                      <input class="uk-input " type="number" name="quantity" value="1" min="1" max="99">
                                      <!-- style="width: 30px; " -->
                                    </form>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Livraison :</td>
                                  <td>Offert</td>
                                </tr>
                                <tr>
                                  <td>Total TTC</td>
                                  <td class="uk-text-bold">68,00€</td>
                                </tr>
                              </tbody>
                            </table>
                            <div class="uk-flex uk-flex-center uk-light uk-margin-small-bottom">
                              <a href="# " class="uk-button uk-button-default uk-text-center uk-width-wsmall">Acheter</a>
                            </div>
                          </div>
                          <div class="uk-flex uk-flex-center uk-flex-middle uk-text-center uk-margin-v@-m">
                            <div>
                              <div>
                                <p class="uk-margin-remove">Tarif abonnés</p>
                              </div>
                              <div>
                                <p class="uk-text-large uk-text-bold">48,00 €</p>
                              </div>
                              <div class="uk-margin-small-bottom">
                                <a class="uk-button uk-button-default uk-width-wsmall" href="# ">Connexion</a>
                              </div>
                              <div>
                                <a class="uk-button uk-button-default uk-width-wsmall" href="# ">S'abonner</a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <h4 class="uk-card-title ">Notes</h4>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dicta est vel praesentium ipsum
                          sequi quae ut ex. Ratione incidunt consequuntur sed, aliquid eum recusandae voluptate minus vitae
                          officiis nihil? Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum quibusdam esse rerum,
                          corporis iure natus voluptas nulla minima ea consequuntur, quaerat quod totam tempore, perspiciatis
                          reiciendis commodi maxime eos aliquam! Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                          Blanditiis odio incidunt qui temporibus, facere enim, quia dolor labore earum molestias ea odit
                          assumenda officia praesentium autem soluta reiciendis aut consequuntur.
                        </p>
                      </div>
                    </div>

                  </div>
                </article>
              </div>

            </div>

            <!-- AUDIO PLAYER -->
            <div class="uk-position-bottom uk-position-z-index uk-position-fixed uk-overlay uk-overlay-default uk-padding-remove-v uk-push-right@m">
            <?php include("main_residence_audio-player.php"); ?>
            </div>
          </main>
