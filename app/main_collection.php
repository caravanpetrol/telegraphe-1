    <main id="<?php echo $page_title ?>" class="uk-background-muted uk-padding-v" uk-height-viewport="expand: true">

        <div class="uk-container">
          <h1 class="uk-text-center">Le Telegraphe
            <span>- - - collection - - -</span>
          </h1>
          <div class="uk-child-width-1-3@m uk-child-width-1-2@s uk-grid-match" uk-grid>
            <!-- CARD -->
            <div>
              <div class="uk-card uk-card-small uk-text-center">
                <div class="uk-card-media-top">
                  <a href="residence.php">
                    <img data-src="https://fakeimg.pl/400x400" alt="artiste - titre album" uk-img>
                  </a>
                </div>
                <div class="uk-card-body">

                  <h2>artiste</h2>
                  <h3>titre album</h3>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt incidunt veniam ut numquam, nam nobis aliquam
                    recusandae sequi</p>
                </div>
                <div class="uk-card-footer">
                  <a href="#" class="uk-button uk-button-default uk-margin-small-h">Acheter</a>
                  <a href="residence.php" class="uk-button uk-button-default uk-margin-small-h">Découvrir</a>
                </div>
              </div>
            </div>
          </div>
        </div>

    </main>


