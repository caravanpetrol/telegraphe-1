<!DOCTYPE html>
<html lang="fr">

  <?php
    $page_title = "telegraphe";
    include("inc/head.php");
   ?>

  <body>

    <?php
    include('inc/header.php');
    ?>

    <!-- CONTENT -->
    <div uk-grid class="wrapper" uk-height-viewport="expand: true">
      <div class="uk-width-1-1 content"><div class='uk-width-1-1 uk-margin-remove-top'>


    <div uk-grid class="wrapper" uk-height-viewport="expand: true">
      <div class="uk-width-1-1 content">
        <div class='uk-width-1-1 uk-margin-remove-top'>
          <div>
              <?php include('main_' . $page_title . '.php'); ?>
          </div>
        </div>
      </div>
    </div>


    <?php include('inc/footer.php'); ?>

    <?php include('inc/scripts.php'); ?>

  </body>
</html>
