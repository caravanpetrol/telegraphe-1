<!DOCTYPE html>
<html lang="fr">

  <?php
    $page_title = "index_v2";
    include("inc/head.php");
   ?>

  <body id="<?php echo $page_title ?>" class="<?php echo $page_title ?>">
    <div id="bg_2" class="uk-visible@m">
    </div>
      <?php include('inc/header-v2.php'); ?>

      <!-- CONTENT -->
      <div class="wrapper">
          <?php include('main_' . $page_title . '.php'); ?>
      </div>

      <?php include('inc/footer-v2.php'); ?>

      <?php // include('inc/scripts.php'); ?>
      <?php include('inc/script_v2.php'); ?>

  </body>
</html>
