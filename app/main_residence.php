          <main>

            <div id="<?php echo $page_title ?>" class=" uk-background-primary uk-flex uk-flex-center uk-child-width-1-1 uk-grid-collapse" uk-grid>

              <!-- HEADER -->
              <div>
                <div uk-height-viewport="offset-top: 90px; " uk-parallax="blur: 5" class="uk-flex uk-flex-center uk-background-cover" data-src="assets/residence-brass.jpg" uk-img>
                  <div class="uk-flex uk-flex-center uk-text-center uk-margin-medium-large-top uk-light ">
                    <h1>
                      <span class="uk-heading-primary uk-display-block uk-margin-small-bottom">Le telegraphe</span>
                      <span class="uk-article-meta uk-display-block uk-margin-small-bottom">Collection juin 2018 #1</span>
                      <span class="uk-heading-hero uk-display-block">Brass</span>
                    </h1>
                  </div>
                </div>
              </div>

              <!-- POSTS -->
              <div class="uk-padding-large-bottom">
                <div class="uk-container uk-padding-remove-h@-s uk-container-expand ">

                  <!-- TEXT LARGE -->
                  <section class="uk-section uk-padding-remove-vertical uk-flex uk-flex-center uk-margin-auto">
                    <div uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true " class="uk-flex uk-flex-center uk-text-center uk-margin-medium-top uk-margin-small-h">
                      <p class="uk-width-max-xlarge-xxlarge">Lorem ipsum dolor,
                        sit amet consectetur adipisicing elit. Modi dicta est vel praesentium ipsum sequi quae ut ex. Ratione
                        incidunt <strong>consequuntur sed, aliquid eum recusandae</strong> voluptate minus vitae officiis nihil? Lorem ipsum
                        dolor sit amet consectetur adipisicing elit. Cum quibusdam esse rerum, corporis iure natus voluptas
                        nulla minima ea consequuntur, quaerat quod totam tempore, perspiciatis reiciendis commodi maxime eos
                        aliquam! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Blanditiis odio incidunt qui temporibus,
                        facere enim, quia dolor labore earum molestias ea odit assumenda officia praesentium autem soluta reiciendis
                        aut consequuntur.
                      </p>
                    </div>
                  </section>

                  <hr class="uk-divider uk-divider-large">

                  <!-- IMAGE ONLY (Horizontal) -->
                  <h2>IMAGE ONLY (Horizontal)</h2>
                  <section class="uk-section uk-padding-remove-vertical uk-flex uk-flex-center uk-margin-auto">
                    <div class="uk-width-max uk-full-width-height" uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">
                    <?php include('main_residence_image-h.php'); ?>
                    </div>
                  </section>

                  <hr class="uk-divider uk-divider-large">

                  <!-- TEST FROM STAGING -->
                  <h2>TEST FROM STAGING</h2>
                  <section class="uk-section uk-padding-remove-vertical uk-flex uk-flex-center uk-margin-auto">
                    <div class="uk-width-max uk-full-width-height uk-text-center" uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">
<!--                       <picture class="uk-full-width-height">
                        <source media="(max-width: 639px)" srcset="https://fakeimg.pl/640x480">
                        <source media="(min-width: 640px) and (max-width: 959px)" srcset="https://fakeimg.pl/960x720">
                        <source media="(min-width: 960px) and (max-width: 1199px)" srcset="https://fakeimg.pl/1200x900">
                        <source media="(min-width: 1200px)" srcset="https://fakeimg.pl/1800x1350">
                        <img src="https://fakeimg.pl/960x720" alt="nom de l'evenement">
                      </picture> -->

                      <picture class="uk-full-width-height">
                        <source media="(max-width: 639px)" srcset="https://resize.ovh/r/b6cb0bb0-c198-11e8-951d-cb43607d0ba4/640">
                        <source media="(min-width: 640px) and (max-width: 959px)" srcset="https://resize.ovh/r/b6cb0bb0-c198-11e8-951d-cb43607d0ba4/960">
                        <source media="(min-width: 960px) and (max-width: 1199px)" srcset="https://resize.ovh/r/b6cb0bb0-c198-11e8-951d-cb43607d0ba4/1200">
                        <source media="(min-width: 1200px)" srcset="https://resize.ovh/r/b6cb0bb0-c198-11e8-951d-cb43607d0ba4/1800">
                        <img src="https://resize.ovh/o/b6cb0bb0-c198-11e8-951d-cb43607d0ba4" alt="Résidence #1 BRASS - Rona Hartner">
                      </picture>

                    </div>
                  </section>

                  <hr class="uk-divider uk-divider-large">

                  <!-- IMAGE + TEXT (Horizontal) -->
                  <h2>Post IMAGE + TEXT (Horizontal) </h2>
                  <section class="uk-section uk-padding-remove-vertical uk-flex uk-flex-center uk-margin-auto uk-child-width-1-1 uk-grid-collapse" uk-grid>
                    <div class=" uk-width-max uk-full-width-height" uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">
                      <?php include('main_residence_image-h.php'); ?>
                    </div>
                    <!-- TEXT LARGE -->
                    <div uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true " class="uk-flex uk-flex-center uk-margin-medium-top uk-margin-small-h">
                      <p class="uk-width-max-xlarge-xxlarge ">Lorem ipsum dolor,
                        sit amet consectetur adipisicing elit. Modi dicta est vel praesentium ipsum sequi quae ut ex. Ratione
                        incidunt consequuntur sed, aliquid eum recusandae voluptate minus vitae officiis nihil? Lorem ipsum
                        dolor sit amet consectetur adipisicing elit. Cum quibusdam esse rerum, corporis iure natus voluptas
                        nulla minima ea consequuntur, quaerat quod totam tempore, perspiciatis reiciendis commodi maxime eos
                        aliquam! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Blanditiis odio incidunt qui temporibus,
                        facere enim, quia dolor labore earum molestias ea odit assumenda officia praesentium autem soluta reiciendis
                        aut consequuntur.
                      </p>
                    </div>
                  </section>
                  <hr class="uk-divider uk-divider-large">
                  <!-- IMAGE ONLY (vertical) -->
                  <h2>post IMAGE ONLY (vertical)</h2>
                  <section class="uk-section uk-padding-remove-vertical uk-flex uk-flex-center uk-margin-auto">
                    <div class="uk-width-max uk-full-width-height" uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">
                      <?php include('main_residence_image-v.php'); ?>
                    </div>
                  </section>

                  <hr class="uk-divider uk-divider-large">

                  <!-- IMAGE ONLY + PLAY AUDIO -->
                  <h2>post IMAGE ONLY + PLAY AUDIO</h2>
                  <section class="uk-section uk-padding-remove-vertical uk-flex uk-flex-center uk-margin-auto">
                    <div class="uk-inline uk-light" uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true">
                    <?php include('main_residence_image-h.php'); ?>
                      <a class="uk-position-absolute uk-transform-center uk-icon-button" style="right: 50%; bottom: 50% " href="#" uk-icon="icon: ico-play; ratio: 1"></a>
                    </div>
                  </section>

                  <hr class="uk-divider uk-divider-large">

                  <!-- TEXTE -> IMAGE -->
                  <h2>Post TEXTE -> IMAGE</h2>
                  <section class="uk-section uk-padding-remove-vertical uk-flex uk-flex-center uk-margin-auto">
                    <div class="uk-flex uk-flex-middle uk-child-width-1-1 uk-child-width-1-2@s " uk-grid>

                      <!-- RIGHT -->
                      <div class="uk-flex-last@m">
                        <div class="uk-inline uk-light" uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">
                          <?php include('main_residence_image-v.php'); ?>
                          <a class="uk-position-absolute uk-transform-center uk-icon-button" style="right: 5px; bottom: 5px " href="#
                        " uk-icon="icon: ico-play; ratio: 1 "></a>
                        </div>
                      </div>

                      <!-- LEFT -->
                      <div class="uk-flex uk-flex-right">
                        <div uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true" class="uk-margin-small-h">
                          <p class="uk-width-max-large@-s">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dicta est vel praesentium ipsum
                            sequi quae ut ex. Ratione incidunt consequuntur sed, aliquid eum recusandae voluptate minus vitae
                            officiis nihil? Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum quibusdam esse rerum,
                            corporis iure natus voluptas nulla minima ea consequuntur, quaerat quod totam tempore, perspiciatis
                            reiciendis commodi maxime eos aliquam! Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                            Blanditiis odio incidunt qui temporibus, facere enim, quia dolor labore earum molestias ea odit
                            assumenda officia praesentium autem soluta reiciendis aut consequuntur.
                          </p>
                        </div>
                      </div>
                    </div>
                  </section>

                  <hr class="uk-divider uk-divider-large">

                  <!-- IMAGE -> TEXTE -->
                  <h2>NEW Post IMAGE -> TEXTE </h2>
                  <section class="uk-section uk-padding-remove-vertical uk-flex uk-flex-center uk-margin-auto">
                    <div class="uk-flex uk-flex-middle uk-child-width-1-1 uk-child-width-1-2@s " uk-grid>

                      <!-- LEFT -->
                      <div>
                        <div class="uk-inline uk-light" uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true ">
                          <?php include('main_residence_image-h.php'); ?>
                          <a class="uk-position-absolute uk-transform-center uk-icon-button" style="right: 5px; bottom: 5px " href="#
                        " uk-icon="icon: ico-play; ratio: 1 "></a>
                        </div>
                      </div>

                      <!-- RIGHT -->
                      <div>
                        <div uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true" class="uk-margin-small-h">
                          <p class="uk-width-max-large@-s">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dicta est vel praesentium ipsum
                            sequi quae ut ex. Ratione incidunt consequuntur sed, aliquid eum recusandae voluptate minus vitae
                            officiis nihil? Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum quibusdam esse rerum,
                            corporis iure natus voluptas nulla minima ea consequuntur, quaerat quod totam tempore, perspiciatis
                            reiciendis commodi maxime eos aliquam! Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                            Blanditiis odio incidunt qui temporibus, facere enim, quia dolor labore earum molestias ea odit
                            assumenda officia praesentium autem soluta reiciendis aut consequuntur.
                          </p>
                        </div>
                      </div>
                    </div>
                  </section>

                </div>
                <!-- END container -->
              </div>
              <!-- END POSTS -->

              <hr class="uk-divider uk-divider-large">


              <!-- INFOS CONTENT -->
              <!-- SLIDESHOW FULL -->
              <div class="uk-margin-remove uk-padding-large-top"><!-- uk-margin-remove uk-padding-medium-v -->
                  <!-- <h2>DETAIL OBJET</h2> -->
                <div class="uk-position-relative " uk-slideshow="ratio: 16:9">
                  <ul class="uk-slideshow-items">
                    <li>
                      <img src="https://fakeimg.pl/1600x900" alt=" " uk-cover>
                    </li>
                    <li>
                      <img src="https://fakeimg.pl/1600x900" alt=" " uk-cover>
                    </li>
                    <li>
                      <img src="https://fakeimg.pl/1600x900" alt=" " uk-cover>
                    </li>
                  </ul>
                  <div class="uk-position-bottom-center uk-position-small ">
                    <ul class="uk-dotnav ">
                      <li uk-slideshow-item="0 ">
                        <a href="# ">Item 1</a>
                      </li>
                      <li uk-slideshow-item="1 ">
                        <a href="# ">Item 2</a>
                      </li>
                      <li uk-slideshow-item="2 ">
                        <a href="# ">Item 3</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              <!-- DISQUE -->
              <div class="uk-margin-remove uk-padding-medium-v "><!-- uk-background-primary  -->
                <article class="uk-container uk-container-small ">
                  <div class="uk-child-width-1-2@s uk-child-width-1-1 uk-grid-large" uk-grid>
                    <div>
                      <div class="uk-card uk-card-media ">
                        <div class="uk-card-media-top uk-text-center uk-text-left@s">
                          <div class="uk-box-shadow-bottom uk-box-shadow-small"> <img data-src="https://fakeimg.pl/155x155 "
                              width="155 " height="155 " alt=" " uk-img>
                          </div>
                        </div>
                        <div class="">
                          <table class="uk-table uk-table-xsmall uk-margin-top ">
                            <caption class="uk-margin-medium-top uk-margin-small-medium-bottom ">Label: LE TELEGRAPHE
                              <br>Format: Vinyl, LP, Album</caption>
                            <tbody>
                              <tr>
                                <td>track 1</td>
                                <td>02'20</td>
                              </tr>
                              <tr>
                                <td>track 2</td>
                                <td>15'50</td>
                              </tr>
                              <tr>
                                <td>track 3</td>
                                <td>5'10</td>
                              </tr>
                            </tbody>
                          </table>
                          <h4 class="uk-card-title ">Crédits</h4>
                          <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dicta est vel praesentium ipsum
                            sequi quae ut ex. Ratione incidunt consequuntur sed, aliquid eum recusandae voluptate minus vitae
                            officiis nihil? Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum quibusdam esse rerum,
                            corporis iure natus voluptas nulla minima ea consequuntur, quaerat quod totam tempore, perspiciatis
                            reiciendis commodi maxime eos aliquam! Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                            Blanditiis odio incidunt qui temporibus, facere enim, quia dolor labore earum molestias ea odit
                            assumenda officia praesentium autem soluta reiciendis aut consequuntur.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="uk-card">
                        <div class="uk-child-width-1-1 uk-child-width-1-2@l  uk-grid-collapse uk-margin-medium-bottom" uk-grid>
                          <div class="uk-table-border uk-padding-xsmall uk-margin-auto uk-width-max-medium">
                            <!-- style="max-width: 250px;" -->
                            <table class="uk-table uk-table-small uk-text-small uk-table-middle ">
                              <!-- <caption>Label Telegraphe</caption> -->
                              <tbody>
                                <tr>
                                  <td>Prix :</td>
                                  <td>68,00€</td>
                                </tr>
                                <tr>
                                  <td>Qté :</td>
                                  <td>
                                    <form class="uk-form-blank uk-width-xsmall">
                                      <!-- <input type="number"> -->
                                      <input class="uk-input " type="number" name="quantity" value="1" min="1" max="99">
                                      <!-- style="width: 30px; " -->
                                    </form>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Livraison :</td>
                                  <td>Offert</td>
                                </tr>
                                <tr>
                                  <td>Total TTC</td>
                                  <td class="uk-text-bold">68,00€</td>
                                </tr>
                              </tbody>
                            </table>
                            <div class="uk-flex uk-flex-center uk-light uk-margin-small-bottom">
                              <a href="# " class="uk-button uk-button-default uk-text-center uk-width-wsmall">Acheter</a>
                            </div>
                          </div>
                          <div class="uk-flex uk-flex-center uk-flex-middle uk-text-center uk-margin-v@-l">
                            <div>
                              <div>
                                <p class="uk-margin-remove">Tarif abonnés</p>
                              </div>
                              <div>
                                <p class="uk-text-large uk-text-bold">48,00 €</p>
                              </div>
                              <div class="uk-margin-small-bottom">
                                <a class="uk-button uk-button-default uk-width-wsmall" href="# ">Connexion</a>
                              </div>
                              <div>
                                <a class="uk-button uk-button-default uk-width-wsmall" href="# ">S'abonner</a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <h4 class="uk-card-title ">Notes</h4>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dicta est vel praesentium ipsum
                          sequi quae ut ex. Ratione incidunt consequuntur sed, aliquid eum recusandae voluptate minus vitae
                          officiis nihil? Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum quibusdam esse rerum,
                          corporis iure natus voluptas nulla minima ea consequuntur, quaerat quod totam tempore, perspiciatis
                          reiciendis commodi maxime eos aliquam! Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                          Blanditiis odio incidunt qui temporibus, facere enim, quia dolor labore earum molestias ea odit
                          assumenda officia praesentium autem soluta reiciendis aut consequuntur.
                        </p>
                      </div>
                    </div>

                  </div>
                </article>
              </div>

            </div>

          </main>
          <!-- AUDIO PLAYER -->
          <div class="uk-position-bottom uk-position-z-index uk-position-fixed uk-overlay uk-overlay-default uk-padding-remove-v uk-push-right@m">
          <?php include("main_residence_audio-player.php"); ?>
          </div>
