<nav id="arrow-nav" class="uk-navbar">
  <ul class="uk-iconnav uk-light">
    <li>
      <a class="uk-padding-remove-right " href="#">
        <span uk-icon="icon: arrow-left; ratio: 1.3 "></span>
      </a>
    </li>
    <li>
      <a class="uk-padding-remove-right agenda-ico" href="#">
        <span uk-icon="icon: ico-agenda; ratio: .8 "></span>
      </a>
    </li>
    <li>
      <a class="uk-padding-remove-left" href="#">
        <span uk-icon="icon: arrow-right; ratio: 1.3 "></span>
      </a>
    </li>
  </ul>
</nav>
