<div class="">
  <div class="uk-flex uk-flex-middle">
    <!-- <div class="uk-text-uppercase uk-text-small">Partager</div> -->
    <div class="uk-margin-small-left uk-light">
      <a class="uk-padding-remove-horizontal social_twitter" href="#" data-url="{{ current_url }}">
        <span class="uk-icon-button " uk-icon="icon: twitter "></span>
      </a>
    </div>
    <div class="uk-margin-small-left uk-light">
      <a class="uk-padding-remove-horizontal social_facebook" href="#" data-url="{{ current_url }}">
        <span class="uk-icon-button " uk-icon="icon: facebook "></span>
      </a>
    </div>
    <div class="uk-margin-small-left uk-light">
      <a class="uk-padding-remove-horizontal social_email" href="#" data-url="{{ current_url }}">
        <span class="uk-icon-button " uk-icon="icon: mail "></span>
      </a>
    </div>
    <div class="uk-margin-small-left uk-light">
      <a class="uk-padding-remove-horizontal social_pinterest" href="#" data-url="{{ current_url }}">
        <span class="uk-icon-button " uk-icon="icon: pinterest "></span>
      </a>
    </div>
  </div>
</div>
