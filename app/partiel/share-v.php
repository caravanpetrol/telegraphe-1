<nav id="share">
  <ul class="uk-iconnav uk-iconnav-vertical uk-text-center uk-light">
    <li>
      <a class="uk-padding-remove-horizontal " href="#">
        <span class="uk-icon-button " uk-icon="icon: twitter "></span>
      </a>
    </li>
    <li>
      <a class="uk-padding-remove-horizontal " href="#">
        <span class="uk-icon-button " uk-icon="icon: facebook "></span>
      </a>
    </li>
    <li>
      <a class="uk-padding-remove-horizontal " href="#">
        <span class="uk-icon-button " uk-icon="icon: pinterest "></span>
      </a>
    </li>
    <li>
      <a class="uk-padding-remove-horizontal " href="#">
        <span class="uk-icon-button " uk-icon="icon: mail "></span>
      </a>
    </li>
  </ul>
  <p class="uk-visible@m uk-text-small uk-transform-vertical uk-text-uppercase uk-margin-medium-top">Partager</p>
</nav>
