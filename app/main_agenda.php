<div class="uk-container uk-container-medium uk-padding-v@m uk-padding-remove-h@-m">
  <main id="<?php echo $page_title ?>" class="uk-child-width-1-1 uk-child-width-1-5@m uk-grid-xsmall uk-grid-collapse@-m" uk-grid>
    <section class="odd-even">
      <div class="uk-height-1-1 uk-card uk-card-default uk-card-small uk-padding-xsmall-v@-m">
        <div uk-grid class="uk-grid-collapse">
          <div class="uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle uk-flex-center uk-text-right@m uk-flex-right@m uk-height-xsmall">
            <div class="uk-card-header uk-height-xsmall ">
              <div>
                <span class="moment-week-day">mardi</span>
                <span class="moment-month-day">26</span>
                <span class="moment-month">sept</span>
                <span class="moment-hour">20:30</span>
                <time datetime="2018-11-06 20:30 ">
                </time>
              </div>
            </div>
          </div>

          <div class="uk-visible@s uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle">
            <div class="uk-inline-clip uk-transition-toggle" tabindex="0">
              <a href="agenda-detail.php"><img class="uk-transition-scale-up uk-transition-opaque" src="https://fakeimg.pl/300x220"
                  alt=""></a>
            </div>
            <!-- <div>
              <img data-src="https://fakeimg.pl/300x220" alt="" uk-img="" src="https://fakeimg.pl/300x220"
                  width="300" height="220">
            </div> -->
          </div>

          <div class="uk-width-expand uk-width-1-1@m uk-flex uk-flex-middle">

            <div class="uk-card-body  ">


              <h2>
                <a class="" href="agenda-detail.php">Artiste ArtisteArtiste Artiste </a>
              </h2>
              <h3 class="type ">Concert sjdghdghfdkjhgdjkhg</h3>

            </div>

          </div>

          <div class="uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle uk-min-height-xxsmall@m">
            <div class="uk-position-bottom@m">
              <div class="uk-flex uk-flex-bottom">
                <div class="uk-card-footer uk-text-center uk-text-left@m">
                  <a href="agenda-detail.php" class="uk-button uk-button-danger">
                    <span class="uk-icon" uk-icon="icon: ico-abonnement"></span>
                    <span class="txt uk-visible@m">réserver</span>
                  </a>
<!--                 <style>
                span.txt { opacity:0; padding-right:0; max-width:0; display:inline-block; transition:.3s; }
        a:hover span.txt { opacity:1; padding-right:.2em; max-width:80px; transition:.3s; }
                </style> -->

                  <p class="info uk-display-block">Reste 3 places</p>

                </div>
              </div>
            </div>
            <!-- <a class="uk-button uk-button-default uk-width-wsmall" href="agenda-detail.php">S'abonner</a> -->
          </div>
        </div>
      </div>
    </section>
    <section class="odd-even">
      <div class="uk-height-1-1 uk-card uk-card-default uk-card-small uk-padding-xsmall-v@-m">
        <div uk-grid class="uk-grid-collapse">
          <div class="uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle uk-flex-center uk-text-right@m uk-flex-right@m">
            <div class="uk-card-header ">
              <div>
                <span class="moment-week-day">mardi</span>
                <span class="moment-month-day">26</span>
                <span class="moment-month">sept</span>
                <span class="moment-hour">20:30</span>
                <time datetime="2018-11-06 20:30 ">
                </time>
              </div>
            </div>
          </div>

          <div class=" uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle"><!-- uk-visible@s -->
            <div>
              <div>
                <a href="agenda-detail.php">
                  <picture class="uk-full-width-height">
                    <source media="(max-width: 639px)" srcset="https://fakeimg.pl/200x200">
                    <source media="(min-width: 640px)" srcset="https://fakeimg.pl/600x440">
                    <img src="https://fakeimg.pl/600x440" alt="nom de l'evenement">
                  </picture>
                </a>
              </div>
            </div>
          </div>

          <div class="uk-width-expand uk-width-1-1@m uk-flex uk-flex-middle">
            <div>
              <div class="uk-card-body  ">
                <div>

                  <h2>
                    <a class="" href="agenda-detail.php">Artiste ArtisteArtiste Artiste </a>
                  </h2>
                  <h3 class="type ">Concert sjdghdghfdkjhgdjkhg</h3>
                </div>
              </div>
            </div>
          </div>

          <div class="uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle uk-flex-center uk-min-height-xxsmall@m">
            <div class="uk-position-bottom@m">
              <div class="uk-flex uk-flex-bottom">
                <div class="uk-card-footer uk-text-center uk-text-left@m">
                <a uk-toggle="cls: uk-transition-toggle; mode: media; media: @m" href="agenda-detail.php" class="uk-button uk-button-danger "><!-- uk-transition-toggle -->
                    <span class="uk-icon " uk-icon="icon: ico-abonnement"></span>
                    <span class="uk-transition-fade uk-visible@m">réserver</span>
                  </a>
                  <p class="info uk-display-block">Reste 3 places</p>
                  <!-- <span class=" uk-badge  ">complet</span> -->
                  <!-- uk-visible@s  uk-position-center-right -->

                </div>
              </div>
            </div>
            <!-- <a class="uk-button uk-button-default uk-width-wsmall" href="agenda-detail.php">S'abonner</a> -->
          </div>
        </div>
      </div>
    </section>
    <section class="odd-even">
      <div class="uk-height-1-1 uk-card uk-card-default uk-card-small uk-padding-xsmall-v@-m">
        <div uk-grid class="uk-grid-collapse">
          <div class="uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle uk-flex-center uk-text-right@m uk-flex-right@m">
            <div class="uk-card-header ">
              <div>
                <span class="moment-week-day">mardi</span>
                <span class="moment-month-day">26</span>
                <span class="moment-month">sept</span>
                <span class="moment-hour">20:30</span>
                <time datetime="2018-11-06 20:30 ">
                </time>
              </div>
            </div>
          </div>

          <div class="uk-visible@s uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle">
            <div>
              <div>
                <a href="agenda-detail.php"><img data-src="https://fakeimg.pl/300x220" alt="" uk-img="" src="https://fakeimg.pl/300x220"
                    width="300" height="220"></a>
              </div>
            </div>
          </div>

          <div class="uk-width-expand uk-width-1-1@m uk-flex uk-flex-middle">
            <div>
              <div class="uk-card-body  ">
                <div>

                  <h2>
                    <a class="" href="agenda-detail.php">Artiste ArtisteArtiste Artiste </a>
                  </h2>
                  <h3 class="type ">Concert sjdghdghfdkjhgdjkhg</h3>
                </div>
              </div>
            </div>
          </div>

          <div class="uk-width-1-4 uk-width-1-6@s uk-width-1-1@m uk-flex uk-flex-middle uk-min-height-xxsmall@m">
            <div class="uk-position-bottom@m">
              <div class="uk-flex uk-flex-bottom">
                <div class="uk-card-footer">
                  <a class="uk-button uk-button-danger uk-button-small " href="agenda-detail.php">réserver</a>
                  <p class="info uk-display-inline-block">Reste 3 places</p>
                  <!-- <span class=" uk-badge  ">complet</span> -->
                  <!-- uk-visible@s  uk-position-center-right -->

                </div>
              </div>
            </div>
            <!-- <a class="uk-button uk-button-default uk-width-wsmall" href="agenda-detail.php">S'abonner</a> -->
          </div>
        </div>
      </div>
    </section>
    <section class="odd-even">
      <div class="uk-height-1-1 uk-card uk-card-default uk-card-small uk-padding-xsmall-v@-m">
        <div uk-grid class="uk-grid-collapse">
          <div class="uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle uk-flex-center uk-text-right@m uk-flex-right@m">
            <div class="uk-card-header ">
              <div>
                <span class="moment-week-day">mardi</span>
                <span class="moment-month-day">26</span>
                <span class="moment-month">sept</span>
                <span class="moment-hour">20:30</span>
                <time datetime="2018-11-06 20:30 ">
                </time>
              </div>
            </div>
          </div>

          <div class="uk-visible@s uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle">
            <div>
              <div>
                <a href="agenda-detail.php"><img data-src="https://loremflickr.com/g/300/220/paris,girl/all" alt="" uk-img="" src="https://loremflickr.com/g/300/220/paris,girl/all"
                    width="300" height="220"></a>
              </div>
            </div>
          </div>

          <div class="uk-width-expand uk-width-1-1@m uk-flex uk-flex-middle">
            <div>
              <div class="uk-card-body  ">
                <div>

                  <h2>
                    <a class="" href="agenda-detail.php">Artiste ArtisteAste </a>
                  </h2>
                  <h3 class="type ">Concert sdjkhg</h3>
                </div>
              </div>
            </div>
          </div>

          <div class="uk-width-1-4 uk-width-1-6@s uk-width-1-1@m uk-flex uk-flex-middle uk-min-height-xxsmall@m">
            <div class="uk-position-bottom@m">
              <div class="uk-flex uk-flex-bottom">
                <div class="uk-card-footer">

                  <span class=" uk-badge  ">complet</span>
                  <!-- uk-visible@s  uk-position-center-right -->

                </div>
              </div>
            </div>
            <!-- <a class="uk-button uk-button-default uk-width-wsmall" href="agenda-detail.php">S'abonner</a> -->
          </div>
        </div>
      </div>
    </section>
    <section class="odd-even">
      <div class="uk-height-1-1 uk-card uk-card-default uk-card-small uk-padding-xsmall-v@-m">
        <div uk-grid class="uk-grid-collapse">
          <div class="uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle uk-flex-center uk-text-right@m uk-flex-right@m">
            <div class="uk-card-header ">
              <div>
                <span class="moment-week-day">mardi</span>
                <span class="moment-month-day">26</span>
                <span class="moment-month">sept</span>
                <span class="moment-hour">20:30</span>
                <time datetime="2018-11-06 20:30 ">
                </time>
              </div>
            </div>
          </div>

          <div class="uk-visible@s uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle">
            <div>
              <div>
                <a href="agenda-detail.php"><img data-src="https://loremflickr.com/g/300/220/paris,girl/all?random=3" alt="" uk-img="" src="https://fakeimg.pl/300x220"
                    width="300" height="220"></a>
              </div>
            </div>
          </div>

          <div class="uk-width-expand uk-width-1-1@m uk-flex uk-flex-middle">
            <div>
              <div class="uk-card-body  ">
                <div>

                  <h2>
                    <a class="" href="agenda-detail.php">Artiste </a>
                  </h2>
                  <h3 class="type ">Concert </h3>
                </div>
              </div>
            </div>
          </div>

          <div class="uk-width-1-4 uk-width-1-6@s uk-width-1-1@m uk-flex uk-flex-middle uk-min-height-xxsmall@m">
            <div class="uk-position-bottom@m">
              <div class="uk-flex uk-flex-bottom">
                <div class="uk-card-footer">
                  <a class="uk-button uk-button-danger uk-button-small " href="agenda-detail.php">réserver</a>

                  <!-- <span class=" uk-badge  ">complet</span> -->
                  <!-- uk-visible@s  uk-position-center-right -->

                </div>
              </div>
            </div>
            <!-- <a class="uk-button uk-button-default uk-width-wsmall" href="agenda-detail.php">S'abonner</a> -->
          </div>
        </div>
      </div>
    </section>
    <section class="odd-even">
      <div class="uk-height-1-1 uk-card uk-card-default uk-card-small uk-padding-xsmall-v@-m">
        <div uk-grid class="uk-grid-collapse">
          <div class="uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle uk-flex-center uk-text-right@m uk-flex-right@m">
            <div class="uk-card-header ">
              <div>
                <span class="moment-week-day">mardi</span>
                <span class="moment-month-day">26</span>
                <span class="moment-month">sept</span>
                <span class="moment-hour">20:30</span>
                <time datetime="2018-11-06 20:30 ">
                </time>
              </div>
            </div>
          </div>

          <div class="uk-visible@s uk-width-1-6 uk-width-1-1@m uk-flex uk-flex-middle">
            <div>
              <div>
                <a href="agenda-detail.php"><img data-src="https://fakeimg.pl/300x220" alt="" uk-img="" src="https://fakeimg.pl/300x220"
                    width="300" height="220"></a>
              </div>
            </div>
          </div>

          <div class="uk-width-expand uk-width-1-1@m uk-flex uk-flex-middle">
            <div>
              <div class="uk-card-body  ">
                <div>

                  <h2>
                    <a class="" href="agenda-detail.php">Artiste ArtisteArtiste Artiste </a>
                  </h2>
                  <h3 class="type ">Concert sjdghdghfdkjhgdjkhg</h3>
                </div>
              </div>
            </div>
          </div>

          <div class="uk-width-1-4 uk-width-1-6@s uk-width-1-1@m uk-flex uk-flex-middle uk-min-height-xxsmall@m">
            <div class="uk-position-bottom@m">
              <div class="uk-flex uk-flex-bottom">
                <div class="uk-card-footer">
                  <a class="uk-button uk-button-danger uk-button-small " href="agenda-detail.php">réserver</a>
                  <p class="info uk-display-inline-block">Reste 3 places</p>
                  <!-- <span class=" uk-badge  ">complet</span> -->
                  <!-- uk-visible@s  uk-position-center-right -->

                </div>
              </div>
            </div>
            <!-- <a class="uk-button uk-button-default uk-width-wsmall" href="agenda-detail.php">S'abonner</a> -->
          </div>
        </div>
      </div>
    </section>








  </main>
</div>
