# UIKit 3 template Telegraphe

## Installation

```bash
npm install
```

## Edit SCSS

- Use `app/index.php` as home page
- Edit `app/scss/app.scss` to override variables & `app/scss/custom.scss` for custom mixin overwrites

```bash
# SASS build command
sass --watch app/scss/app.scss:app/css/app.css --style compressed
```

- the generated css file is in `app/css/app.css`

## Build assets (uikit icons)

- Place new icons in the folder `app/assets/uikit/custom/icons`
- run `npm run build:uikit` (will generate uikit-icons.min.js & uikit.min.js in app/assets/js)
